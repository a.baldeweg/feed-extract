# baldeweg/feed-extract

Extracts RSS, Podcast and Atom feeds from HTML.

## Installation

```shell
composer require baldeweg/feed-extract
```

## Usage

Accepts HTML and returns an array of feeds.

```php
use Baldeweg\FeedExtract\Extract;

$extract = new Extract();
$extract->fetchFeeds($html);
```

When the input is e.g.

```php
<link rel="alternate" type="application/rss+xml" title="Title" href="Feed1">
```

it results in the following array

```php
[
  [
    'type' => 'application/rss+xml',
    'title' => 'Title',
    'href' => 'Feed1'
  ]
]
```

Security: You have to validate all the delivered data!

### Dev

- bin/build - Reports and tests
- bin/lint - Checks for code standard violations and fixes them partially
- bin/phpunit - Runs the phpunit tests
- bin/tag VERSION - Sets a git tag
