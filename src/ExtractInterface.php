<?php

/*
 * This script is part of baldeweg/feed-extract
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\FeedExtract;

interface ExtractInterface
{
    public function fetchFeeds(string $data): ?array;
}
