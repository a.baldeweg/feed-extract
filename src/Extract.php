<?php

/*
 * This script is part of baldeweg/feed-extract
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\FeedExtract;

use Baldeweg\FeedExtract\ExtractInterface;

class Extract implements ExtractInterface
{
    public function fetchFeeds(string $data): ?array
    {
        preg_match_all(
            '/<link rel="alternate" type="(.*)" title="(.*)" href="(.*)"(.*)>/U',
            $data,
            $feeds
        );

        return isset($feeds[3]) ? $this->process($feeds) : null;
    }

    protected function process(array $data): array
    {
        $counter = count($data[0]);
        $links = [];
        for ($i = 0; $i < $counter; $i++) {
            $links[] = [
                'type' => strip_tags($data[1][$i]),
                'title' => strip_tags($data[2][$i]),
                'href' => strip_tags($data[3][$i])
            ];
        }

        return $links;
    }
}
