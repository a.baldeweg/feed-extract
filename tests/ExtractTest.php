<?php

/*
 * This script is part of baldeweg/feed-extract
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\FeedExtract\Tests;

use Baldeweg\FeedExtract\Extract;
use PHPUnit\Framework\TestCase;

class ExtractTest extends TestCase
{
    /**
     * @dataProvider data
     */
    public function testFetchFeeds($html, $expected)
    {
        $parser = new Extract();
        $result = $parser->fetchFeeds($html);

        $this->assertEquals($result, $expected);
    }

    public function data()
    {
        return [
            [
                '<link rel="alternate" type="application/rss+xml" title="Title" href="Feed1">',
                [
                    [
                        'type' => 'application/rss+xml',
                        'title' => 'Title',
                        'href' => 'Feed1'
                    ]
                ]
            ],
            [
                '<link rel="alternate" type="application/rss+xml" title="Title" href="Feed1" />',
                [
                    [
                        'type' => 'application/rss+xml',
                        'title' => 'Title',
                        'href' => 'Feed1'
                    ]
                ]
            ],
            [
                '<link rel="alternate" type="application/rss+xml" title="Title" href="Feed1"/>',
                [
                    [
                        'type' => 'application/rss+xml',
                        'title' => 'Title',
                        'href' => 'Feed1'
                    ]
                ]
            ],
            [
                '<link rel="alternate" type="application/atom+xml" title="Title" href="Feed1">',
                [
                    [
                        'type' => 'application/atom+xml',
                        'title' => 'Title',
                        'href' => 'Feed1'
                    ]
                ]
            ],
            [
                '<link rel="alternate" type="application/rss+xml" title="Title" href="Feed1"><link rel="alternate" type="application/rss+xml" title="Title" href="Feed2">',
                [
                    [
                        'type' => 'application/rss+xml',
                        'title' => 'Title',
                        'href' => 'Feed1'
                    ],
                    [
                        'type' => 'application/rss+xml',
                        'title' => 'Title',
                        'href' => 'Feed2'
                    ]
                ]
            ]
        ];
    }
}
